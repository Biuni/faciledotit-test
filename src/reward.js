const getReward = (carny, config) => {
  let reward = 0 // No rewards reached
  const targetsFromTop = sortByReward([...config.targets])
  for (const el of targetsFromTop) {
    if (
      carny.tokensSold >= el.tokensSold &&
      carny.carouselRides >= el.carouselRides &&
      carny.avgTokenPrice >= el.avgTokenPrice
    ) {
      reward = (el.recompenseForTokenSold * carny.tokensSold).toFixed(2)
      break
    }
  }
  return +reward
}

/*
 * Sort the targets using
 * the rewards.
 */
const sortByReward = (obj) => obj.sort((a, b) => b.recompenseForTokenSold - a.recompenseForTokenSold)

module.exports = getReward
