const getTarget = (carny, config) => {
  let target = -1 // No targets reached
  const targetsFromTop = sortByReward([...config.targets])
  for (const [index, el] of targetsFromTop.entries()) {
    if (
      carny.tokensSold >= el.tokensSold &&
      carny.carouselRides >= el.carouselRides &&
      carny.avgTokenPrice >= el.avgTokenPrice
    ) {
      target = targetsFromTop.length - index
      break
    }
  }
  return +target
}

/*
 * Sort the targets using
 * the rewards.
 */
const sortByReward = (obj) => obj.sort((a, b) => b.recompenseForTokenSold - a.recompenseForTokenSold)

module.exports = getTarget
