const React = require('react')
const Layout = require('./layouts/default')

class Result extends React.Component {
  render () {
    return (
      <Layout title={this.props.title}>
        <div className='container'>
          <div className='jumbotron jumbotron-fluid text-center py-3'>
            <h1 className='display-4'>Risultato</h1>
          </div>
          <div className='row'>
            <div className='alert alert-success text-center col-md-5' role='alert'>
              <em>Ricompensa: </em>
              <h1>{this.props.reward}</h1>
            </div>
            <div className='col-md-2 text-center'>
              <img className='mw-100 w-75' src='https://image.flaticon.com/icons/svg/744/744922.svg' alt='Reward' />
            </div>
            <div className='alert alert-info text-center col-md-5' role='alert'>
              <em>Target corrispondente: </em>
              <h1>{(this.props.target === -1) ? '-' : this.props.target}</h1>
            </div>
          </div>
          <div className='text-right'>
            <a href='/carny-reward'>&laquo; Ripeti</a>
          </div>
        </div>
      </Layout>
    )
  }
}

module.exports = Result
