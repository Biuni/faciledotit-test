const React = require('react')
const Layout = require('./layouts/default')
const FormTarget = require('./components/formTarget')

class Target extends React.Component {
  render () {
    return (
      <Layout title={this.props.title}>
        <div className='container'>
          <div className='jumbotron jumbotron-fluid text-center py-3'>
            <h1 className='display-4'>Nuovo target</h1>
          </div>
          {this.props.result === 1 &&
            <div className='alert alert-success' role='alert'>Target aggiunto!</div>
          }
          <FormTarget />
          <div className='text-right'>
            <a href='/'>&laquo; Homepage</a>
          </div>
        </div>
      </Layout>
    )
  }
}

module.exports = Target
