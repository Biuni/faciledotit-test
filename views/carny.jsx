const React = require('react')
const Layout = require('./layouts/default')
const FormCarny = require('./components/formCarny')

class Carny extends React.Component {
  render () {
    return (
      <Layout title={this.props.title}>
        <div className='container'>
          <div className='jumbotron jumbotron-fluid text-center py-3'>
            <h1 className='display-4'>Calcolo del compenso</h1>
          </div>
          <FormCarny />
          <div className='text-right'>
            <a href='/'>&laquo; Homepage</a>
          </div>
        </div>
      </Layout>
    )
  }
}

module.exports = Carny
