const React = require('react')
const Layout = require('./layouts/default')

class Homepage extends React.Component {
  render () {
    return (
      <Layout title={this.props.title}>
        <div className='container'>
          <div className='jumbotron jumbotron-fluid text-center py-3'>
            <img className='mw-100 w-25 px-5' src='https://image.flaticon.com/icons/svg/486/486173.svg' alt='Carousel' />
            <h1 className='display-4'>Carousel</h1>
            <p className='lead px-2'>Applicazione per il calcolo del compenso mensile dei giostrai.</p>
            <a href='/new-target' className='btn btn-outline-secondary'>Nuovo target</a>&nbsp;
            <a href='/carny-reward' className='btn btn-outline-success'>Calcolo del compenso</a>
          </div>
        </div>
      </Layout>
    )
  }
}

module.exports = Homepage
