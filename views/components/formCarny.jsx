const React = require('react')

class FormCarny extends React.Component {
  constructor (props) {
    super(props)
    this.monthDate = [
      { value: '01-01', name: 'Gennaio' },
      { value: '02-01', name: 'Febbraio' },
      { value: '03-01', name: 'Marzo' },
      { value: '04-01', name: 'Aprile' },
      { value: '05-01', name: 'Maggio' },
      { value: '06-01', name: 'Giugno' },
      { value: '07-01', name: 'Luglio' },
      { value: '08-01', name: 'Agosto' },
      { value: '09-01', name: 'Settembre' },
      { value: '10-01', name: 'Ottobre' },
      { value: '11-01', name: 'Novembre' },
      { value: '12-01', name: 'Dicembre' }
    ]
    this.yearDate = [
      { value: '2019' },
      { value: '2018' },
      { value: '2017' },
      { value: '2016' },
      { value: '2015' }
    ]
  }

  render () {
    return (
      <form action='result' method='POST'>
        <div className='form-row'>
          <div className='form-group col-md-6'>
            <label>Mese</label>
            <select className='form-control' name='carnyMonth'>
              {this.monthDate.map((e, key) => <option key={key} value={e.value}>{e.name}</option>)}
            </select>
          </div>
          <div className='form-group col-md-6'>
            <label>Anno</label>
            <select className='form-control' name='carnyYear'>
              {this.yearDate.map((e, key) => <option key={key} value={e.value}>{e.value}</option>)}
            </select>
          </div>
        </div>
        <hr />
        <div className='form-group'>
          <label>Valori raggiunti</label>
          <input type='number' className='form-control my-1' id='tokensSold' name='tokensSold' placeholder='Gettoni Venduti' autoComplete='off' step='any' required />
          <input type='number' className='form-control my-1' id='carouselRides' name='carouselRides' placeholder='Giri di giostra' autoComplete='off' step='any' required />
          <input type='number' className='form-control my-1' id='avgTokenPrice' name='avgTokenPrice' placeholder='Prezzo medio per gettone' autoComplete='off' step='any' required />
        </div>
        <button type='submit' className='btn btn-primary'>Avvia Calcolo</button>
      </form>
    )
  }
}

module.exports = FormCarny
