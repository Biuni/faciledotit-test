/* eslint-env mocha */
const chai = require('chai')
const expect = chai.expect

// Modules and JSON for testing
const getTarget = require('../src/target')
const configuration = {
  'startDate': '2018-11-01',
  'targets': [{
    'tokensSold': 100,
    'carouselRides': 200,
    'avgTokenPrice': 1.00,
    'recompenseForTokenSold': 0.10
  }, {
    'tokensSold': 180,
    'carouselRides': 400,
    'avgTokenPrice': 1.10,
    'recompenseForTokenSold': 0.12
  }, {
    'tokensSold': 300,
    'carouselRides': 750,
    'avgTokenPrice': 1.20,
    'recompenseForTokenSold': 0.30
  }, {
    'tokensSold': 500,
    'carouselRides': 1500,
    'avgTokenPrice': 1.50,
    'recompenseForTokenSold': 0.50
  }]
}

describe('Target calculation', () => {
  it('Test the targets in configuration as example of carnies', (done) => {
    let output = []
    for (const el of configuration.targets) { output.push(getTarget(el, configuration)) }
    // Expected result
    expect(output).to.eql([1, 2, 3, 4])
    done()
  })
  it('Example of target 1 or less', (done) => {
    let output = []
    const carnies = [{
      'tokensSold': 101,
      'carouselRides': 201,
      'avgTokenPrice': 1.01
    }, {
      'tokensSold': 99,
      'carouselRides': 201,
      'avgTokenPrice': 1.01
    }, {
      'tokensSold': 101,
      'carouselRides': 199,
      'avgTokenPrice': 1.01
    }, {
      'tokensSold': 101,
      'carouselRides': 201,
      'avgTokenPrice': 0.99
    }]
    for (const el of carnies) { output.push(getTarget(el, configuration)) }
    // Expected result
    expect(output).to.eql([1, -1, -1, -1])
    done()
  })
  it('Example of target 2 or 1', (done) => {
    let output = []
    const carnies = [{
      'tokensSold': 181,
      'carouselRides': 401,
      'avgTokenPrice': 1.11
    }, {
      'tokensSold': 179,
      'carouselRides': 401,
      'avgTokenPrice': 1.11
    }, {
      'tokensSold': 181,
      'carouselRides': 399,
      'avgTokenPrice': 1.11
    }, {
      'tokensSold': 181,
      'carouselRides': 401,
      'avgTokenPrice': 1.09
    }]
    for (const el of carnies) { output.push(getTarget(el, configuration)) }
    // Expected result
    expect(output).to.eql([2, 1, 1, 1])
    done()
  })
  it('Example of target 3 or 2', (done) => {
    let output = []
    const carnies = [{
      'tokensSold': 301,
      'carouselRides': 751,
      'avgTokenPrice': 1.21
    }, {
      'tokensSold': 299,
      'carouselRides': 751,
      'avgTokenPrice': 1.21
    }, {
      'tokensSold': 301,
      'carouselRides': 749,
      'avgTokenPrice': 1.21
    }, {
      'tokensSold': 301,
      'carouselRides': 751,
      'avgTokenPrice': 1.19
    }]
    for (const el of carnies) { output.push(getTarget(el, configuration)) }
    // Expected result
    expect(output).to.eql([3, 2, 2, 2])
    done()
  })
  it('Example of target 4 or 3', (done) => {
    let output = []
    const carnies = [{
      'tokensSold': 501,
      'carouselRides': 1501,
      'avgTokenPrice': 1.51
    }, {
      'tokensSold': 499,
      'carouselRides': 1501,
      'avgTokenPrice': 1.51
    }, {
      'tokensSold': 501,
      'carouselRides': 1499,
      'avgTokenPrice': 1.51
    }, {
      'tokensSold': 501,
      'carouselRides': 1501,
      'avgTokenPrice': 1.49
    }]
    for (const el of carnies) { output.push(getTarget(el, configuration)) }
    // Expected result
    expect(output).to.eql([4, 3, 3, 3])
    done()
  })
})
