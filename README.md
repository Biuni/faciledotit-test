# Facile.it - Carousel Software
Il software, utile a calcolare il compenso mensile dei giostrai della rete Carousel, è stato sviluppato utilizzando Javascript (ES6) su piattaforma **Node JS**. L'applicazione dispone inoltre di un'interfaccia web, la quale è stata sviluppata con **ExpressJS** e **React**.

## Architettura Software
L'architettura del software è composta nel seguente modo:

*  Nel modulo `data` sono presenti le strutture dati utili al calcolo dei compensi.
*  Nel modulo `src` troviamo la logica vera e propria del sistema, ovvero tutte le funzioni per il calcolo della ricompensa e del target raggiunto dai vari giostrai.
*  Nel modulo `view` sono invece inserite le viste, ovvero tutto quello che compone l'interfaccia dell'applicazione.
*  Nel modulo `test` vengono scritti gli *unit test*.

I vari moduli sono orchestrati tramite il file `index.js`.

## Installazione
Per installare le dipendenze eseguire il comando:
```bash
$ yarn
```
o in alternativa
```bash
$ npm install
```

## Esecuzione
Per avviare il software eseguire il comando:
```bash
$ yarn start
```
o in alternativa
```bash
$ npm start
```
---
Se invece si vuole avviare il software in modalità sviluppo, eseguire il comando:
```bash
$ yarn dev:start
```
o in alternativa
```bash
$ npm run dev:start
```

## Test
Per avviare i test eseguire il comando:
```bash
$ yarn test
```
o in alternativa
```bash
$ npm test
```

## Informazioni per il calcolo
Prima di effettuare il calcolo della ricompensa, la configurazione viene ordinata in base al valore del campo `recompenseForTokenSold`, dando un "valore" più alto ai target che offrono una ricompensa maggiore, anche nell'eventualità che siano più facili da raggiungere.
Ad esempio:
```javascript
  [{
    "tokensSold": 40,
    "carouselRides": 100,
    "avgTokenPrice": 1.3,
    "recompenseForTokenSold": 0.8
  }, {
    "tokensSold": 2000,
    "carouselRides": 2000,
    "avgTokenPrice": 10,
    "recompenseForTokenSold": 0.7
  }, {
    "tokensSold": 100,
    "carouselRides": 300,
    "avgTokenPrice": 1.3,
    "recompenseForTokenSold": 0.6
  }]
```
diventerà:
```javascript
  [{
    "tokensSold": 100,
    "carouselRides": 300,
    "avgTokenPrice": 1.3,
    "recompenseForTokenSold": 0.6
  }, {
    "tokensSold": 2000,
    "carouselRides": 2000,
    "avgTokenPrice": 10,
    "recompenseForTokenSold": 0.7
  }, {
    "tokensSold": 40,
    "carouselRides": 100,
    "avgTokenPrice": 1.3,
    "recompenseForTokenSold": 0.8
  }]
```