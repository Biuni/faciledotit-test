const exReact = require('express-react-views')
const bodyParser = require('body-parser')
const app = require('express')()
const chalk = require('chalk')
const path = require('path')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.set('views', path.join(__dirname, '/views'))
app.set('view engine', 'jsx')
app.engine('jsx', exReact.createEngine())

const getReward = require('./src/reward')
const getTarget = require('./src/target')
const db = require('./data/db')
const port = process.env.PORT || 3000

/*
 * Express Route
 * GET /
 * Homepage of the website.
 */
app.get('/', (req, res) => {
  res.render('index', { title: 'Homepage | Carousel' })
})

/*
 * Express Route
 * GET /new-target
 * Add new target.
 */
app.get('/new-target', (req, res) => {
  res.render('target', { title: 'Creazione del target | Carousel' })
})

/*
 * Express Route
 * GET /carny-reward
 * Insert data to generate rewards.
 */
app.get('/carny-reward', (req, res) => {
  res.render('carny', { title: 'Calcolo del compenso | Carousel' })
})

/*
 * Express Route
 * POST /result
 * Print the result of the earned rewards.
 */
app.post('/result', (req, res) => {
  const result = req.body
  const err = 'Nessun target corrispondente al mese!'
  const startDate = `${result.carnyYear}-${result.carnyMonth}`
  const target = db.get('list').find({ startDate: startDate }).value()

  res.render('result', {
    title: 'Risultato | Carousel',
    reward: (target === undefined) ? err : getReward(result, target),
    target: (target === undefined) ? err : getTarget(result, target)
  })
})

/*
 * Express Route
 * POST /add
 * Add the target.
 */
app.post('/add', (req, res) => {
  const startDate = `${req.body.carnyYear}-${req.body.carnyMonth}`
  const target = db.get('list')
    .find({ startDate: startDate })
    .value()

  // If the startDate is not found in
  // the list, create a new target.
  if (target !== undefined) {
    db.get('list').remove({ startDate: startDate }).write()
    target.targets.push({
      tokensSold: +req.body.tokensSold,
      carouselRides: +req.body.carouselRides,
      avgTokenPrice: +req.body.avgTokenPrice,
      recompenseForTokenSold: +req.body.recompenseForTokenSold
    })
    db.get('list').push(target).write()
  } else {
  // If the startDate is found in
  // the list, update the target.
    db.get('list').push({
      startDate: startDate,
      targets: [{
        tokensSold: +req.body.tokensSold,
        carouselRides: +req.body.carouselRides,
        avgTokenPrice: +req.body.avgTokenPrice,
        recompenseForTokenSold: +req.body.recompenseForTokenSold
      }]
    }).write()
  }

  res.render('add', {
    title: 'Nuovo Target | Carousel',
    result: 1
  })
})

/*
 * Express Listener
 * Binds and listens for connections
 * on the specified host and port.
 */
app.listen(port, () =>
  console.log('\n' +
  chalk.green('App listening on: ') +
  chalk.magenta(`http://localhost:${port}`))
)
